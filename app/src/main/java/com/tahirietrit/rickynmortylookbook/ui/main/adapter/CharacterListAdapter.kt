package com.tahirietrit.rickynmortylookbook.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tahirietrit.rickynmortylookbook.data.entities.Character
import com.tahirietrit.rickynmortylookbook.databinding.CharacterListItemBinding
import com.tahirietrit.rickynmortylookbook.ui.main.MainFragmentDirections

class CharacterListAdapter : ListAdapter<Character, CharacterListAdapter.ItemViewholder>(HiresDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewholder {
        val binding = CharacterListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
        )
        binding.model = ItemViewholder(binding)
        return binding.model as ItemViewholder
    }

    override fun onBindViewHolder(holder: ItemViewholder, position: Int) {
        holder.bind(getItem(position))
    }

    class ItemViewholder(private val binding: CharacterListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Character) = with(itemView) {
            binding.character = item
        }

        fun showDetailData(character: Character) {
            val direction = MainFragmentDirections.actiontOpenDetailFragment(character)
            binding.root.findNavController().navigate(direction)
        }
    }
}

class HiresDiffCallback : DiffUtil.ItemCallback<Character>() {

    override fun areItemsTheSame(oldItem: Character, newItem: Character): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Character, newItem: Character): Boolean {
        return oldItem == newItem
    }
}
