package com.tahirietrit.rickynmortylookbook.ui.main

import androidx.lifecycle.MutableLiveData
import com.gunaya.demo.demomeow.presentation.base.BaseViewModel
import com.tahirietrit.rickynmortylookbook.data.entities.Character
import com.tahirietrit.rickynmortylookbook.data.repositories.CharactersRepository
import com.tahirietrit.rickynmortylookbook.utils.RequestResult
import com.tahirietrit.rickynmortylookbook.utils.SingleLiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel (val repository: CharactersRepository) : BaseViewModel() {

    val charactersList = MutableLiveData<List<Character>>()
    val showError = SingleLiveEvent<String>()
    val showLoading = SingleLiveEvent<Boolean>()
    init {
        loadCharacters()
    }

    fun loadCharacters(){
        launch {
            showLoading.value = true
            val result = withContext(Dispatchers.IO) { repository.getCharacters() }
            when (result) {
                is RequestResult.Success ->{
                    showLoading.value = false
                    charactersList.value = result.data.results
                }
                is RequestResult.Error -> {
                    showLoading.value = false
                    showError.value = result.exception.message
                }
            }
        }
    }

}
