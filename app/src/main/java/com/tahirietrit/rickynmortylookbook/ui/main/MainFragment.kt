package com.tahirietrit.rickynmortylookbook.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.tahirietrit.rickynmortylookbook.databinding.MainFragmentBinding
import com.tahirietrit.rickynmortylookbook.ui.main.adapter.CharacterListAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    private val viewModel: MainViewModel by viewModel()
    private lateinit var characterListAdapter: CharacterListAdapter

    lateinit var mainFragmentBinding: MainFragmentBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        mainFragmentBinding = MainFragmentBinding.inflate(inflater)
        return mainFragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        mainFragmentBinding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.loadCharacters()
        }
        characterListAdapter = CharacterListAdapter()
        mainFragmentBinding.charactersRecyclerview.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = characterListAdapter
        }
    }

    private fun observeViewModel() {
        viewModel.charactersList.observe(viewLifecycleOwner, Observer { characterList ->
            mainFragmentBinding.swipeRefreshLayout.isRefreshing = false
            mainFragmentBinding.progressCircular.visibility = View.GONE
            characterListAdapter.submitList(characterList)
        })
        viewModel.showError.observe(viewLifecycleOwner, Observer { showError ->
            Snackbar.make(mainFragmentBinding.root, showError, Snackbar.LENGTH_SHORT).show()
        })
        viewModel.showLoading.observe(viewLifecycleOwner, Observer { showLoading ->
            if (showLoading) {
                mainFragmentBinding.progressCircular.visibility = View.VISIBLE
            } else {
                mainFragmentBinding.progressCircular.visibility = View.GONE
            }
        })
    }

}
