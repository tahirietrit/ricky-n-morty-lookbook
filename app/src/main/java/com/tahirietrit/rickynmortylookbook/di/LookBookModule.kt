package com.tahirietrit.rickynmortylookbook.di

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.tahirietrit.rickynmortylookbook.data.remote.RickynMortyApi
import com.tahirietrit.rickynmortylookbook.data.remote.RickynMortyApi.Companion.ENDPOINT
import com.tahirietrit.rickynmortylookbook.data.repositories.CharactersRepository
import com.tahirietrit.rickynmortylookbook.ui.main.MainViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val lookbookModule = module {
    single { createHttpLoggingInterceptor() }
    single {
        createWebService<RickynMortyApi>(okHttpClient = createHttpClient(httpLoggingInterceptor = get()),
                baseUrl = ENDPOINT)
    }

    factory<CharactersRepository> { CharactersRepository(rickynMortyApi = get()) }

    viewModel { MainViewModel(repository = get()) }

}

fun createHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()
}

fun createHttpLoggingInterceptor(): HttpLoggingInterceptor {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    return httpLoggingInterceptor
}

inline fun <reified T> createWebService(
        okHttpClient: OkHttpClient, baseUrl: String): T {
    val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(okHttpClient)
            .build()
    return retrofit.create(T::class.java)
}