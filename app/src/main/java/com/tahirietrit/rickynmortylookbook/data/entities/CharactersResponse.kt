package com.tahirietrit.rickynmortylookbook.data.entities

import com.squareup.moshi.Json
data class CharactersResponse(

		@Json(name="results")
	val results: List<Character> = mutableListOf<Character>(),

		@Json(name="info")
	val info: Info? = null
)