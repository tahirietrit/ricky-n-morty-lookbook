package com.tahirietrit.rickynmortylookbook.data.remote

import com.tahirietrit.rickynmortylookbook.data.entities.CharactersResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface RickynMortyApi {

    companion object {
        const val ENDPOINT = "https://rickandmortyapi.com/"
    }

    @GET("api/character/")
    fun getCharacters(): Deferred<CharactersResponse>

}