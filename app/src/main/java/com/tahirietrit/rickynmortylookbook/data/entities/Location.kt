package com.tahirietrit.rickynmortylookbook.data.entities

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Location(

	@Json(name="name")
	val name: String? = null,

	@Json(name="url")
	val url: String? = null
) : Parcelable