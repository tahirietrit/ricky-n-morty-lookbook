package com.tahirietrit.rickynmortylookbook.data.repositories

import com.tahirietrit.rickynmortylookbook.data.entities.CharactersResponse
import com.tahirietrit.rickynmortylookbook.data.remote.RickynMortyApi
import com.tahirietrit.rickynmortylookbook.utils.RequestResult


class CharactersRepository(val rickynMortyApi: RickynMortyApi) {
    suspend fun getCharacters(): RequestResult<CharactersResponse> {
        return try {
            val result = rickynMortyApi.getCharacters().await()
            RequestResult.Success(result)
        } catch (ex: Exception) {
            RequestResult.Error(ex)
        }
    }

}