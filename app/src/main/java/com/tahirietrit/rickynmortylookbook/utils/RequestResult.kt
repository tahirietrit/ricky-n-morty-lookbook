package com.tahirietrit.rickynmortylookbook.utils

sealed class RequestResult<out T : Any> {
    class Success<out T : Any>(val data: T) : RequestResult<T>()
    class Error(val exception: Throwable) : RequestResult<Nothing>()
}