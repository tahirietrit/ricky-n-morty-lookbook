package com.tahirietrit.rickynmortylookbook

import android.app.Application
import com.tahirietrit.rickynmortylookbook.di.lookbookModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class RickynMortyLookbook: Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@RickynMortyLookbook)
            modules(lookbookModule)
        }
    }
}