package com.tahirietrit.rickynmortylookbook

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        println("result: " + range(arrayListOf(5, 4, 3, 9, 7, 2, 4, 61, 7, 8), 61))
    }

    private fun range(arr: List<Int>, sum: Int): Pair<Int, Int> {
        var potentialResult = arr[0]
        var start = 0
        var i = 1
        while (i <= arr.size) {
            while (potentialResult > sum && start < i - 1) {
                potentialResult -= arr[start]
                start++
            }
            if (potentialResult == sum) {
                val end = i - 1
                return Pair(start, end)
            }
            if (i < sum) potentialResult += arr[i]
            i++
        }
        return Pair(-1, -1)
    }

}

// Input:
// let array = [5, 4, 3, 9, 7, 2, 4, 61, 7, 88]
// let sum = 16

// Output:
// range (start-index, length)

// Examples:

// func test() {
//         example 1
//         print(findSum(array: [5, 4, 3, 9, 7, 2, 4, 61, 7, 8], sum: 12)) // (0, 3)
//
//         example 2
//         print(findSum(array: [5, 4, 3, 9, 7, 2, 4, 61, 7, 8], sum: 7)) // (1, 2)

//         example 3
//         print(findSum(array: [5, 4, 3, 9, 7, 2, 4, 61, 7, 8], sum: 16)) // (1, 3)

//         exampl
//         example 4
//         print(findSum(array: [5, 4, 3, 9, 7, 2, 4, 61, 7, 8], sum: 61)) // (7, 1)
//
//         example 5
//         print(findSum(array: [5, 4, 3, 9, 7, 2, 4, 61, 7, 8], sum: 68)) // (7, 2)
//
//         example 6
//         print(findSum(array: [5, 4], sum: 68)) // (-1, -1)
// }

// func findSum(array:[UInt], sum:UInt) -> (Int, Int) {
//         //insert algorithm here
//         return (-1, -1)
// }
